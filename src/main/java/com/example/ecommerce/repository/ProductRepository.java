package com.example.ecommerce.repository;

import com.example.ecommerce.entity.ProductMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<ProductMaster, Integer> {


    @Query(value = "select * from product_master pm where pm.name in ?1", nativeQuery = true)
    List<ProductMaster> getProductDetailsByName(List<String> name);
}
