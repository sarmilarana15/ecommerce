package com.example.ecommerce.repository;

import com.example.ecommerce.dto.ShopperInterface;
import com.example.ecommerce.entity.ShopperMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ShopperRepository extends JpaRepository< ShopperMaster, Integer> {


    @Query(value = "Select sm.name AS shopperName, pm.name AS productName, sm.relevancy_score AS score from shopper_master sm join product_master pm on pm.id = sm.product_id " +
            "where sm.name =:shopperId and (COALESCE(:category) IS NUll  = true or (pm.category in (:category))) " +
            "and (COALESCE(:brand) IS NUll = true or (pm.brand in (:brand))) LIMIT :limit", nativeQuery = true)
    List<ShopperInterface> fetchShopperDetails(String shopperId, String category, String brand, Integer limit);
}
