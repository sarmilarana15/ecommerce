package com.example.ecommerce.dao;

import com.example.ecommerce.dto.FilterDTO;
import com.example.ecommerce.dto.ShopperInterface;
import com.example.ecommerce.entity.ProductMaster;
import com.example.ecommerce.entity.ShopperMaster;
import com.example.ecommerce.repository.ProductRepository;
import com.example.ecommerce.repository.ShopperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EcommerceDAO {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ShopperRepository shopperRepository;

    public void saveShopperDetails(List<ShopperMaster> master){
        shopperRepository.saveAll(master);
    }

    public List<ProductMaster> getProductDetails(List<String> names){
        return productRepository.getProductDetailsByName(names);
    }

    public List<ShopperInterface> fetchShopperDetails(FilterDTO filterDTO){
        return shopperRepository.fetchShopperDetails(filterDTO.getShopperId(), filterDTO.getCategory(), filterDTO.getBrand(), filterDTO.getLimit());

    }



}
