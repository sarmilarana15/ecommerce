package com.example.ecommerce.controller;

import com.example.ecommerce.dto.FilterDTO;
import com.example.ecommerce.service.ExternalService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/external")
@RequiredArgsConstructor
@Slf4j
public class ExternalController {

    @Autowired
    ExternalService externalService;

    @PostMapping("/fetch")
    public ResponseEntity<Object> fetchShopperDetails(@RequestBody FilterDTO filters){
        return  externalService.getShopperDetails(filters);

    }

}
