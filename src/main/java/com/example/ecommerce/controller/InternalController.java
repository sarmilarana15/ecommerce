package com.example.ecommerce.controller;

import com.example.ecommerce.dto.ShopperMetaDataDTO;
import com.example.ecommerce.service.InternalService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/internal")
@RequiredArgsConstructor
@Slf4j
public class InternalController {

    @Autowired
    private InternalService internalService;

    @PostMapping("/create")
    public ResponseEntity<Object> createShopperData(@RequestBody ShopperMetaDataDTO request){
        return  internalService.createShopperMetaData(request);
    }
}
