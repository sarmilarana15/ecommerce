package com.example.ecommerce.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class FilterDTO {
    @NotNull(message = "shopperId cannot be null!")
    @NotBlank(message = "shopperId cannot be blank!")
    private String shopperId;
    private String category;
    private String brand;
    private Integer limit;
}
