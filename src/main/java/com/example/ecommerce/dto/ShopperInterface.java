package com.example.ecommerce.dto;

public interface ShopperInterface {

    String getShopperName();

    String getProductName();

    Double getScore();

}
