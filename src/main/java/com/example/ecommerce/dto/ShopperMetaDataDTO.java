package com.example.ecommerce.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ShopperMetaDataDTO {

    private String shopperId;

    private List<ProductDetails> shelf;
}
