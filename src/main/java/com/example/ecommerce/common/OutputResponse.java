package com.example.ecommerce.common;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OutputResponse {

    private String message;
    private String status;
    private Object data;

    public static OutputResponse of(String message, String status, Object data) {
        return new OutputResponse(  status, data );
    }

    public static OutputResponse of(String message, String status) {
        return new OutputResponse( status, message );
    }

    public static OutputResponse ok(Object data) {
        OutputResponse outputResponse = new OutputResponse();
        outputResponse.status = "success";
        outputResponse.data = data;
        return outputResponse;
    }

    public static OutputResponse error(String message) {
        OutputResponse outputResponse = new OutputResponse();
        outputResponse.status = "error";
        outputResponse.message = message;
        return outputResponse;
    }

    private OutputResponse(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public static OutputResponse of(String message, Object data) {
        return new OutputResponse( message, "Success", data );
    }

    private OutputResponse(String message, String status, Object data) {
        this.message = message;
        this.status = status;
        this.data = data;
    }

    private OutputResponse(String status, Object data) {
        this.data = data;
        this.status = status;
    }
}
