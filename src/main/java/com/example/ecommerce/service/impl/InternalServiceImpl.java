package com.example.ecommerce.service.impl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.ecommerce.common.OutputResponse;
import com.example.ecommerce.dao.EcommerceDAO;
import com.example.ecommerce.dto.ProductDetails;
import com.example.ecommerce.dto.ShopperMetaDataDTO;
import com.example.ecommerce.entity.ProductMaster;
import com.example.ecommerce.entity.ShopperMaster;
import com.example.ecommerce.service.InternalService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class InternalServiceImpl implements InternalService {

    @Autowired
    private EcommerceDAO ecommerceDAO;

    public ResponseEntity<Object> createShopperMetaData(ShopperMetaDataDTO shopperMetaDataDTO){
        try{

            List<String> productDetails =  shopperMetaDataDTO.getShelf().stream()
                    .map(ProductDetails::getProductId)
                    .toList();
            List<ProductMaster> productMasterList = ecommerceDAO.getProductDetails(productDetails);

            Map<String, Double> productVsScore = shopperMetaDataDTO.getShelf().stream()
                    .collect(Collectors.toMap(ProductDetails::getProductId, ProductDetails::getRelevancyScore));

            List<ShopperMaster> shopperData = productMasterList.stream()
                            .map(ps ->  ShopperMaster.builder()
                                    .shopperName(shopperMetaDataDTO.getShopperId())
                                    .productMasterDetails(ps)
                                    .score(productVsScore.get(ps.getProductName()))
                                    .createdBy("System")
                                    .createdTime(LocalDateTime.now()).build())
                    .toList();
            ecommerceDAO.saveShopperDetails(shopperData);
            return ResponseEntity.ok(OutputResponse.of("Success", "Stored the Shopper Details"));



        }catch (Exception e){
            log.error("Unable to Store shopper Meta Data:", e);
            return ResponseEntity.internalServerError().body(OutputResponse.error("Failed to store the data!!"));

        }

    }

    public static String timeStamp() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM HH:mm");
        return LocalDateTime.now().format(formatter);
    }
}
