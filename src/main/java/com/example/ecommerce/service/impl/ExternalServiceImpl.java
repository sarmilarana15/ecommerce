package com.example.ecommerce.service.impl;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.ecommerce.common.OutputResponse;
import com.example.ecommerce.dao.EcommerceDAO;
import com.example.ecommerce.dto.FilterDTO;
import com.example.ecommerce.dto.ProductDetails;
import com.example.ecommerce.dto.ShopperInterface;
import com.example.ecommerce.dto.ShopperMetaDataDTO;
import com.example.ecommerce.service.ExternalService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ExternalServiceImpl implements ExternalService {

	@Autowired
	EcommerceDAO ecommerceDAO;

	public ResponseEntity<Object> getShopperDetails(FilterDTO filters) {
		try {
			log.info("Filters:{}", filters);
			if (Objects.isNull(filters.getLimit())) {
				filters.setLimit(10);
			}
			List<ShopperInterface> masterList = ecommerceDAO.fetchShopperDetails(filters);

			List<ProductDetails> productDetails = masterList.stream().map(shops -> ProductDetails.builder()
					.productId(shops.getProductName()).relevancyScore(shops.getScore()).build()).toList();
			return ResponseEntity.ok(OutputResponse.of("Success",
					ShopperMetaDataDTO.builder().shopperId(filters.getShopperId()).shelf(productDetails).build()));

		} catch (Exception e) {
			log.error("Unable to get shopper Meta Data:", e);
			return ResponseEntity.internalServerError().body(OutputResponse.error("Failed to get the data!!"));

		}

	}
}
