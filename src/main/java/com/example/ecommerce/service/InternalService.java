package com.example.ecommerce.service;

import org.springframework.http.ResponseEntity;

import com.example.ecommerce.dto.ShopperMetaDataDTO;

public interface InternalService {
	
	ResponseEntity<Object> createShopperMetaData(ShopperMetaDataDTO shopperMetaDataDTO);

}
