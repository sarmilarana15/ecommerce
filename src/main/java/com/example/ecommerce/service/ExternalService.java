package com.example.ecommerce.service;

import org.springframework.http.ResponseEntity;

import com.example.ecommerce.dto.FilterDTO;

public interface ExternalService {

	ResponseEntity<Object> getShopperDetails(FilterDTO filters);
}
